package com.devcamp.j080.person.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j080.person.models.Address;
import com.devcamp.j080.person.models.Animals;
import com.devcamp.j080.person.models.Duck;
import com.devcamp.j080.person.models.Fish;
import com.devcamp.j080.person.models.Person;
import com.devcamp.j080.person.models.Professor;
import com.devcamp.j080.person.models.Student;
import com.devcamp.j080.person.models.Subject;
import com.devcamp.j080.person.models.Worker;
import com.devcamp.j080.person.models.Zebra;

@CrossOrigin
@RestController
public class PersonController {
    @GetMapping("person")
    public ArrayList<Person> getList(){
    ArrayList<Person> lstPerson = new ArrayList<Person>();

    Student student1 = new Student(19, "Male", "John Doe", new Address(), 2, new ArrayList<Subject>(){
        {
            add(new Subject("Triet hoc MacLenin", 3, new Professor(70, "Male", "Tung Hoang", new Address(), 10000)));
            add(new Subject("Xac suat thong ke", 10, new Professor(50, "Female", "Jason Staham", new Address(), 5000)));
        }
    },
    new ArrayList<Animals>(){
        {
            add(new Duck(1, "Male", "white"));
            add(new Fish(2, "Female", 1, true));
        }
    });
    Student student2 = new Student(20, "Female", "Husky", new Address(), 5, new ArrayList<Subject>(){
        {
            add(new Subject("English", 4, new Professor(48, "Male", "Alaska", new Address(), 8000)));
            add(new Subject("Spanish", 10, new Professor(55, "Female", "Black Widow", new Address(), 20000)));
        }
    }, new ArrayList<Animals>(){
        {
            add(new Duck(2, "Male", "yellow"));
            add(new Zebra(3, "Male", true));
        }
    });

        Address add1 = new Address("Lang", "HN", "VN", 10000);
        Address add2 = new Address("Cau Giay", "HN", "VN", 10000);
        Address add3 = new Address("Long An", "Long An", "VN", 20000);
        Address add4 = new Address("Ngo Quyen", "HP", "VN", 30000);

        Worker worker1 = new Worker(30, "Male", "Hoang", add1, 5000000);
        Worker worker2 = new Worker(28, "Female", "Huong", add2, 5000000);
        
        Professor pro1 = new Professor(50, "Male", "Bui Van Toan", add3, 300000);
        Professor pro2 = new Professor(58, "Female", "Truong Kim Huong", add4, 5000000);

        lstPerson.add(student1);
        lstPerson.add(student2);
        lstPerson.add(worker1);
        lstPerson.add(worker2);
        lstPerson.add(pro1);
        lstPerson.add(pro2);
        return lstPerson;
    }
    }
