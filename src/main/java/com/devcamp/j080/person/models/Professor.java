package com.devcamp.j080.person.models;

public class Professor extends Person{
    private int salary;

    public void teaching(){
        
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Professor() {
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        
    }

    public Professor(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }
}
