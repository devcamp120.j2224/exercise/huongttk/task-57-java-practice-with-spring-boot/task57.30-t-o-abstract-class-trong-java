package com.devcamp.j080.person.models;

import java.util.ArrayList;

public class Student extends Person {
    private int studentId;
    ArrayList<Subject> listSubject = new ArrayList();
    ArrayList<Animals> listPet = new ArrayList();

    public void doHomework(){
        System.out.println("Student has to do Homework...");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Student is eating vegetable....");
    }

    public Student(int studentId, ArrayList<Subject> listSubject) {
        this.studentId = studentId;
        this.listSubject = listSubject;
    }

    public Student(int age, String gender, String name, Address address, int studentId,
            ArrayList<Subject> listSubject) {
        super(age, gender, name, address);
        this.studentId = studentId;
        this.listSubject = listSubject;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public ArrayList<Subject> getListSubject() {
        return listSubject;
    }

    public void setListSubject(ArrayList<Subject> listSubject) {
        this.listSubject = listSubject;
    }

    public ArrayList<Animals> getListPet() {
        return listPet;
    }

    public void setListPet(ArrayList<Animals> listPet) {
        this.listPet = listPet;
    }

    public Student(int studentId, ArrayList<Subject> listSubject, ArrayList<Animals> listPet) {
        this.studentId = studentId;
        this.listSubject = listSubject;
        this.listPet = listPet;
    }

    public Student(int age, String gender, String name, Address address, int studentId, ArrayList<Subject> listSubject,
            ArrayList<Animals> listPet) {
        super(age, gender, name, address);
        this.studentId = studentId;
        this.listSubject = listSubject;
        this.listPet = listPet;
    }
    
}
