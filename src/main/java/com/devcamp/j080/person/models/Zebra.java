package com.devcamp.j080.person.models;

public class Zebra extends Animals{
    private boolean is_wild;
    

    public Zebra(boolean is_wild) {
        this.is_wild = is_wild;
    }
    public Zebra(int age, String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }
    
    public void run(){
        System.out.println("Zebra is running...");
    }

    public boolean isIs_wild() {
        return is_wild;
    }
    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Zebra is mammal!");
    }
}