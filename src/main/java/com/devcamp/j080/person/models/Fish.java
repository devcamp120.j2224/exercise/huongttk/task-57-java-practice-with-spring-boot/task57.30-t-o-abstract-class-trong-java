package com.devcamp.j080.person.models;

public class Fish extends Animals{
    private int size;
    private boolean canEat;

    public void swim(){
        System.out.println("Fish is swimming...");
    }

    public Fish(int size, boolean canEat) {
        this.size = size;
        this.canEat = canEat;
    }

    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Fish is not mammal!");
    
}
}
