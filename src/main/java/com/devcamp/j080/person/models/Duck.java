package com.devcamp.j080.person.models;

public class Duck extends Animals {
    private String beakColor;

    public void swim(){
        System.out.println("Duck is swimming...");
    }
    public void quack(){
        System.out.println("Duck is quacking...");
    }
    public Duck(String beakColor) {
        this.beakColor = beakColor;
    }
    public Duck(int age, String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }
    public String getBeakColor() {
        return beakColor;
    }
    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }
    
    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println(" Duck is not Mammal!");
    }
}
