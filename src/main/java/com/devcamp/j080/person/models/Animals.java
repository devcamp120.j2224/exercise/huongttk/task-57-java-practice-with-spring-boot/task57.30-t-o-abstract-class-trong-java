package com.devcamp.j080.person.models;

public abstract class Animals {
    private int age;
    private String gender;

    public abstract void isMammal();
    
    public void mate(){
        System.out.println("Dong vat co the ket doi");
    }

    public Animals() {
    }

    public Animals(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
