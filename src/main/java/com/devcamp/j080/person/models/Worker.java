package com.devcamp.j080.person.models;

public class Worker extends Person {
    private int salary;

    public void working(){
        System.out.println("Worker is working...");
    }

    @Override
    public void eat() {
        // TODO Auto-generated method stub
        System.out.println("Worker have to eating hard...");
    }

    public Worker(int salary) {
        this.salary = salary;
    }

    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
}
