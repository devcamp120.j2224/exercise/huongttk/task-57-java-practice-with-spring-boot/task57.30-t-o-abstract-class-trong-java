package com.devcamp.j080.person.models;

public class Subject {
    private String subTitle;
    private int subId;
    Professor teacher;
    public Subject() {
    }
    public Subject(String subTitle, int subId, Professor teacher) {
        this.subTitle = subTitle;
        this.subId = subId;
        this.teacher = teacher;
    }
    public String getSubTitle() {
        return subTitle;
    }
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
    public int getSubId() {
        return subId;
    }
    public void setSubId(int subId) {
        this.subId = subId;
    }
    public Professor getTeacher() {
        return teacher;
    }
    public void setTeacher(Professor teacher) {
        this.teacher = teacher;
    }
}
